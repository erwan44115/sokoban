package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FacadeModele {
    Modele modele = new ModeleConcret();
    Modele modeleCoup = new ModeleCoup();

    public void move(int x, int y) {
        modeleCoup.move(x,y);
    }

    public void reset() {
        modeleCoup.reset();
    }

    public void undo() {
        modeleCoup.undo();
    }

    public void redo() {
        modeleCoup.redo();
    }

    public void replay(int indiceEtat) {
        modeleCoup.replay(indiceEtat);
    }

    public void incNbCoup(){
        modeleCoup.incNbCoup();
    }

    public int getNbCoup(){
        return modeleCoup.getNbCoup();
    }

    public void setNbCoup(int ind,int val){ modeleCoup.setNbCoup(ind,val); }

    public ArrayList<String> getListNbCoup(){ return modeleCoup.getListNbCoup(); }
    public String getRecord(int val){ return  modeleCoup.getRecord(val); }

    public void loadLevel(String chemin){ modeleCoup.loadLevel(chemin);}
    public Map<Integer,List<List<String>>> getListeNiveaux(){
        return modeleCoup.getListeNiveaux();
    }
    public void setNiveauActuel(int level){
        modeleCoup.setNiveauActuel(level);
    }

    public List<List<String>> getEtat() {
        return modeleCoup.getNiveauActuel();
    }

    public List<List<String>> getNiveauActuel(){
        return modeleCoup.getNiveauActuel();
    }

    public int getSokoPositionX(){
        return modeleCoup.getSokoPositionX();
    }

    public int getSokoPositionY(){
        return modeleCoup.getSokoPositionY();
    }
    public int getNumeroLevelActuel(){
        return modeleCoup.getNumeroLevelActuel();
    }
    public Boolean getVictoire(){
        return modeleCoup.getVictoire();
    }
    public int getLevel(){ return modeleCoup.getLevel(); }


}
