package sample;

public class ModeleCoup  extends ModeleConcret implements Modele{

    ModeleConcret modele;
    int nbCoup =0;

    public ModeleCoup() {

    }



    public void move(int x, int y) {
        String posPlusXY=niveauActuel.get(sokoPositionY+y).get(sokoPositionX+x);
        String devantCaisse=niveauActuel.get(sokoPositionY+y+y).get(sokoPositionX+x+x);
        String pos=niveauActuel.get(sokoPositionY).get(sokoPositionX);
        // si il n'y a pas de mur devant
        if(!posPlusXY.contains("#")){
            // si il y a une caisse devant
            if(posPlusXY.contains("$") || posPlusXY.contains("*")){
                // si la caisse est bougeable
                if(!devantCaisse.contains("#") && !devantCaisse.contains("$") && !devantCaisse.contains("*")){
                    // on bouge la cazisse devant
                    if(devantCaisse.contains(".")){
                        niveauActuel.get(sokoPositionY+y+y).set(sokoPositionX+x+x,"*");
                    }
                    else{
                        niveauActuel.get(sokoPositionY+y+y).set(sokoPositionX+x+x,"$");
                    }
                    // puis on deplace soko
                    if(pos.contains("+")){
                        niveauActuel.get(sokoPositionY).set(sokoPositionX,".");
                    }
                    else{
                        niveauActuel.get(sokoPositionY).set(sokoPositionX," ");
                    }
                    //

                    //C'est cela qu'on rajoute dans ce decorateur
                    incNbCoup();

                    //

                    //

                    sokoPositionX+=x;
                    sokoPositionY+=y;
                    pos=niveauActuel.get(sokoPositionY).get(sokoPositionX);
                    if(pos.contains(".") || pos.contains("*")){
                        niveauActuel.get(sokoPositionY).set(sokoPositionX,"+");
                    }
                    else{
                        niveauActuel.get(sokoPositionY).set(sokoPositionX,"@");
                    }
                }
            }
            // si devant il n'y a pas de caisse
            else{
                // on deplace soko
                if(pos.contains("+")){
                     niveauActuel.get(sokoPositionY).set(sokoPositionX,".");
                }
                else{
                    niveauActuel.get(sokoPositionY).set(sokoPositionX," ");
                }

                //

                //C'est cela qu'on rajoute dans ce decorateur
                 incNbCoup();

                //

                //
                sokoPositionX+=x;
                sokoPositionY+=y;
                pos=niveauActuel.get(sokoPositionY).get(sokoPositionX);
                if(pos.contains(".")){
                    niveauActuel.get(sokoPositionY).set(sokoPositionX,"+");
                }
                else{
                    niveauActuel.get(sokoPositionY).set(sokoPositionX,"@");
                }
            }
        }
        if(isWin()){
            victoire=true;
        }
        saveEtat();
    }

}

