package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class IHMFX extends Application implements Observateur {
    VueIHMFX vue;

    public void actualise(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vue.dessine();
            }
        });
    };

    @Override
    public void start(Stage primaryStage) throws Exception{
        Controleur controleur = Controleur.getControleur();
        controleur.abonne(this);

        vue = new VueIHMFX(controleur);
        ControleurIHMFX controleurIHMFX = new ControleurIHMFX(controleur,vue);
        vue.gridPane.setAlignment(Pos.CENTER);
        vue.gridPaneHaut.setAlignment(Pos.CENTER);

        Scene scene = vue.monteurScene.
                setHaut(vue.gridPaneHaut).
                setCentre(vue.gridPane).
                ajoutBas(controleurIHMFX.reset).
                ajoutBas(controleurIHMFX.undo).
                ajoutBas(controleurIHMFX.redo).
                ajoutBas(controleurIHMFX.replay).
                ajoutBas(controleurIHMFX.monteurMenu.button_menu).
                ajoutBas(vue.button_lvl_prec).
                ajoutBas(vue.button_lvl_next).
                setLargeur(1500).
                setHauteur(800).
                retourneScene();


        scene.getStylesheets().clear();
        scene.getStylesheets().add("sokoban.css");

        primaryStage.setScene(scene);
        controleurIHMFX.addKeyHandler(scene);
        primaryStage.setTitle("Sokoban");
        primaryStage.getIcons().add(new Image("images/lock_logo.jpg"));
        primaryStage.show();
    }

    public void lance() {
        launch(new String[]{});
    }
}

