package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Controleur implements Sujet {
    private static Controleur singleton;


    public static Controleur getControleur() {
        if (singleton == null)
            singleton = new Controleur(new FacadeModele());
        return singleton;
    }

    FacadeModele facadeModele;
    ArrayList<Observateur> observateurs = new ArrayList<Observateur>();

    private Controleur(FacadeModele facadeModele) {
        this.facadeModele = facadeModele;
    }

    public void abonne(Observateur observateur) {
        observateurs.add(observateur);
    }

    @Override
    public void notifie() {
        for (Observateur observateur:observateurs)
            observateur.actualise();
    }

    public void move(int x, int y) {
        facadeModele.move(x,y);
        notifie();
    }

    public void reset() {
        facadeModele.reset();
        notifie();
    }

    public void undo() {
        facadeModele.undo();
        notifie();
    }

    public void redo() {
        facadeModele.redo();
        notifie();
    }

    public void replay(){
        new Thread(() -> {
            for (int i = 0; i < facadeModele.getNbCoup()+1; i++) {
                System.out.print("replay ");
                System.out.println(i);

                rejouerCoup(i);
            }
        }).start();
    }

    public void rejouerCoup(int nCoup){
        facadeModele.replay(nCoup);
        notifie();
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadLevel(String chemin){
        facadeModele.loadLevel(chemin);
        notifie();
    }

    public int getLevel(){
        return facadeModele.getLevel();
    }

    public void setLevel(int level){
        facadeModele.setNiveauActuel(level);
        notifie();
    }

    public Map<Integer,List<List<String>>> getListeNiveaux(){
        return facadeModele.getListeNiveaux();
    }

    public void incNbCoup(){
        facadeModele.incNbCoup();
    }

    public int getnbCoup(){
        return facadeModele.getNbCoup();
    }

    public void setNbCoup(int ind,int val){ facadeModele.setNbCoup(ind,val); }

    public ArrayList<String> getListNbCoup(){ return facadeModele.getListNbCoup(); }

    public String getRecord(int val){ return  facadeModele.getRecord(val); }

    public Boolean getVictoire(){
        return facadeModele.getVictoire();
    }

    public CommandeTabInt commandeGetEtat() {
        return new CommandeTabInt() {
            @Override
            public List<List<String>> exec() {
                return facadeModele.getEtat();
            }
        };
    }
}
