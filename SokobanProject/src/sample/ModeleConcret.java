package sample;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ModeleConcret implements Modele {

    Map<Integer,List<List<String>>> listeNiveaux= new HashMap();
    List<List<String>> niveauActuel = new ArrayList();
    List<List<List<String>>> listeEtats = new ArrayList();
    List<List<List<String>>> listeRedo = new ArrayList();
    int sokoPositionX;
    int sokoPositionY;
    int nbCoup=0;
    int niveau=0;
    boolean victoire=false;
    ArrayList<String> listNbCoup =new ArrayList<>();



    public int[][] etat = {{0,0}};

    public int[][] getEtat() {
        return etat;
    }

    public int getLevel(){
        return niveau;
    }

    public int getSokoPositionX(){
        return sokoPositionX;
    }

    public Boolean getVictoire(){
        return victoire;
    }

    public int getSokoPositionY(){
        return sokoPositionY;
    }


    public void move(int x, int y) {
        String posPlusXY=niveauActuel.get(sokoPositionY+y).get(sokoPositionX+x);
        String devantCaisse=niveauActuel.get(sokoPositionY+y+y).get(sokoPositionX+x+x);
        String pos=niveauActuel.get(sokoPositionY).get(sokoPositionX);
        // si il n'y a pas de mur devant
        if(!posPlusXY.contains("#")){
            // si il y a une caisse devant
            if(posPlusXY.contains("$") || posPlusXY.contains("*")){
                // si la caisse est bougeable
                if(!devantCaisse.contains("#") && !devantCaisse.contains("$") && !devantCaisse.contains("*")){
                    // on bouge la cazisse devant
                    if(devantCaisse.contains(".")){
                        niveauActuel.get(sokoPositionY+y+y).set(sokoPositionX+x+x,"*");
                    }
                    else{
                        niveauActuel.get(sokoPositionY+y+y).set(sokoPositionX+x+x,"$");
                    }
                    // puis on deplace soko
                    if(pos.contains("+")){
                        niveauActuel.get(sokoPositionY).set(sokoPositionX,".");
                    }
                    else{
                        niveauActuel.get(sokoPositionY).set(sokoPositionX," ");
                    }
                    sokoPositionX+=x;
                    sokoPositionY+=y;
                    pos=niveauActuel.get(sokoPositionY).get(sokoPositionX);
                    if(pos.contains(".") || pos.contains("*")){
                        niveauActuel.get(sokoPositionY).set(sokoPositionX,"+");
                    }
                    else{
                        niveauActuel.get(sokoPositionY).set(sokoPositionX,"@");
                    }
                }
            }
            // si devant il n'y a pas de caisse
            else{
                // on deplace soko
                if(pos.contains("+")){
                    niveauActuel.get(sokoPositionY).set(sokoPositionX,".");
                }
                else{
                    niveauActuel.get(sokoPositionY).set(sokoPositionX," ");
                }
                sokoPositionX+=x;
                sokoPositionY+=y;
                pos=niveauActuel.get(sokoPositionY).get(sokoPositionX);
                if(pos.contains(".")){
                    niveauActuel.get(sokoPositionY).set(sokoPositionX,"+");
                }
                else{
                    niveauActuel.get(sokoPositionY).set(sokoPositionX,"@");
                }
            }
        }
        if(isWin()){
            victoire=true;
        }
        saveEtat();
    }


    public void incNbCoup(){
        nbCoup++;
    }

    public int getNbCoup(){
        return nbCoup;
    }

    public ArrayList<String> getListNbCoup(){
        return listNbCoup;
    }

    public String getRecord(int val){
        return listNbCoup.get(val);
    }

    @Override
    public void reset() {
        nbCoup=0;
        System.out.println("Level qui n'est pas censer bouger dans hashMap :");
        System.out.println(listeNiveaux.get(1)) ;
        setNiveauActuel(niveau);
    }

    public void undo() {
        if (listeEtats.size()>1) {
            niveauActuel.clear();

            for(int e=0; e<listeEtats.get(listeEtats.size() - 2).size();e++ ){
                List<String> n=new ArrayList();
                for(int f=0; f<listeEtats.get(listeEtats.size() - 2).get(e).size();f++){
                    if(listeEtats.get(listeEtats.size() - 2).get(e).get(f).contains("@") || listeEtats.get(listeEtats.size() - 2).get(e).get(f).contains("+")){
                        sokoPositionX=f;
                        sokoPositionY=e;
                    }
                    n.add(new String(listeEtats.get(listeEtats.size() - 2).get(e).get(f)));
                }
                niveauActuel.add(n);
            }
            listeRedo.add(listeEtats.get(listeEtats.size() - 1));
            listeEtats.remove(listeEtats.size() - 1);
        }
    }

    public void redo(){
        if(listeRedo.size()>0){
            niveauActuel.clear();

            for(int e=0; e<listeRedo.get(listeRedo.size() - 1).size();e++ ){
                List<String> n=new ArrayList();
                for(int f=0; f<listeRedo.get(listeRedo.size() - 1).get(e).size();f++){
                    if(listeRedo.get(listeRedo.size() - 1).get(e).get(f).contains("@") || listeRedo.get(listeRedo.size() - 1).get(e).get(f).contains("+")){
                        sokoPositionX=f;
                        sokoPositionY=e;
                    }
                    n.add(new String(listeRedo.get(listeRedo.size() - 1).get(e).get(f)));
                }
                niveauActuel.add(n);
            }
            listeEtats.add(listeRedo.get(listeRedo.size()-1));
            listeRedo.remove(listeRedo.size()-1);
        }
    }

    public void replay(int indiceEtat){
        if(indiceEtat>=0) {
            if (listeEtats.size() > 1) {
                niveauActuel.clear();
                for (int e = 0; e < listeEtats.get(indiceEtat).size(); e++) {
                    List<String> n = new ArrayList();
                    for (int f = 0; f < listeEtats.get(indiceEtat).get(e).size(); f++) {
                        if (listeEtats.get(indiceEtat).get(e).get(f).contains("@") || listeEtats.get(indiceEtat).get(e).get(f).contains("+")) {
                            sokoPositionX = f;
                            sokoPositionY = e;
                        }
                        n.add(new String(listeEtats.get(indiceEtat).get(e).get(f)));
                    }
                    niveauActuel.add(n);
                }
            }
        }
    }

    public  Map<Integer,List<List<String>>> getListeNiveaux(){
        return listeNiveaux;
    }

    public void setNbCoup(int ind,int val){
        listNbCoup.set(ind,Integer.toString(val));
    }


    // LOAD TOUT LES LEVELS DANS LA MAP
    public void loadLevel(String chemin){
        BufferedReader reader;
        List<List<String>> niveau = new ArrayList();
        int numLevel=1;
        try {
            reader = new BufferedReader(new FileReader(
                    chemin));
            String line = reader.readLine();
            while (line != null) {
               if(line.length()>0) {
                   if (line.charAt(0) != ';') {
                       ArrayList<String> listeLigne = new ArrayList();
                       for (int i = 0; i < line.length(); i++) {
                           String c=""+line.charAt(i);
                           listeLigne.add(c);
                       }
                       niveau.add(listeLigne);

                   } else {

                       listeNiveaux.put(numLevel,new ArrayList(niveau));
                       numLevel += 1;
                       listNbCoup.add("Non réalisé");

                       niveau.clear();
                       //System.out.println(listeNiveaux);
                   }
               }
                line = reader.readLine();

            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*Set<Map.Entry<Integer, List<List<String>>>> setHm = listeNiveaux.entrySet();
        Iterator<Map.Entry<Integer,List<List<String>>>> it2 = setHm.iterator();

        while(it2.hasNext()){

            Map.Entry<Integer, List<List<String>>> e = it2.next();

            System.out.println(e.getKey() + " : " + e.getValue());

        }*/

    }

    public void saveEtat(){
        List<List<String>> etat = new ArrayList();
        for(int e=0; e<niveauActuel.size();e++ ){
            List<String> n=new ArrayList();
            for(int f=0; f<niveauActuel.get(e).size();f++){
                n.add(new String(niveauActuel.get(e).get(f)));
            }
            etat.add(n);
        }
        listeEtats.add(etat);
    }

    // change de niveau
    public void setNiveauActuel(int level){
        victoire=false;
        nbCoup=0;
        niveau=level;
        listeRedo.clear();
        if (level!=0) {
            niveauActuel = new ArrayList();
            // Obligé de faire une copie comme celle ci pour éviter le changement de listeNiveaux
            ArrayList<String> n = new ArrayList<String>();
            for (int e = 0; e < listeNiveaux.get(level).size(); e++) {
                n = new ArrayList<String>();
                for (int f = 0; f < listeNiveaux.get(level).get(e).size(); f++) {
                    System.out.println(listeNiveaux.get(level).get(e).get(f));
                    n.add(listeNiveaux.get(level).get(e).get(f));
                }
                niveauActuel.add(n);
            }
            listeEtats.clear();
            saveEtat();

            for (int i = 0; i < niveauActuel.size(); i++) {

                for (int j = 0; j < niveauActuel.get(i).size(); j++) {
                    if (niveauActuel.get(i).get(j).contains("@") || niveauActuel.get(i).get(j).contains("+")) {
                        sokoPositionX = j;
                        sokoPositionY = i;
                    }
                }
            }
            System.out.println("X : " + sokoPositionX);
            System.out.println("Y : " + sokoPositionY);
        }
    }

    public List<List<String>> getNiveauActuel(){
        return niveauActuel;
    }

    public int getNumeroLevelActuel(){
        return niveau;
    }

    // A FAIRE PAUL
    public Boolean isWin(){
        Boolean cpt=true;
        for(int i=0;i<niveauActuel.size();i++) {
            for (int j = 0; j < niveauActuel.get(i).size(); j++) {
                if(niveauActuel.get(i).get(j).contains("$")){
                    cpt=false;
                }
            }
        }
        return cpt;
    }
}
