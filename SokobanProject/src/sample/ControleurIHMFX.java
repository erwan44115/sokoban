package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import javax.swing.text.LabelView;
import java.io.File;
import java.util.ArrayList;

public class ControleurIHMFX {
    Controleur controleur;
    VueIHMFX vue;
    Button reset;
    Button undo;
    Button redo;
    Button replay;
    int indiceNiveaux;
    ArrayList<Button> listeNiveaux;
    boolean up, down, left, right;
    MonteurMenu monteurMenu;


    ArrayList<MediaPlayer> playlist;

    // cette classe fait le lien entre vue et controleur, on y met les boutons et les events sur les boutons
    ControleurIHMFX(Controleur controleur, VueIHMFX vue) {
        this.controleur = controleur;
        this.vue = vue;
        this.monteurMenu = vue.monteurMenu;

        playlist = new ArrayList<>();

        File dir = new File("src/sons/");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                Media mus = new Media(new File(child.getPath()).toURI().toString());
                playlist.add(new MediaPlayer(mus));
            }
        }

        for (int i = 0; i < playlist.size(); i++) {
            final MediaPlayer player     = playlist.get(i);
            final MediaPlayer nextPlayer = playlist.get((i + 1) % playlist.size());
            player.setVolume(0.2);
            player.setOnEndOfMedia(new Runnable() {
                @Override public void run() {
                    nextPlayer.stop();
                    nextPlayer.play();
                }
            });
        }
        playlist.get(0).stop();
        playlist.get(0).play();


        reset = new Button("Reset");
        reset.setOnAction(new ActionReset());
        reset.setVisible(false);

        undo = new Button("Revenir en arrière");
        undo.setOnAction(new ActionUndo());
        undo.setVisible(false);

        redo = new Button("Revenir en avant");
        redo.setOnAction(new ActionRedo());
        redo.setVisible(false);

        replay = new Button("Rejouer les coups");
        replay.setOnAction(new ActionReplay());
        replay.setVisible(false);

        monteurMenu.button_menu.setOnAction(new ActionMenu());
        monteurMenu.button_start.setOnAction(new ActionStart());
        monteurMenu.button_select.setOnAction(new ActionSelect());
        monteurMenu.button_option.setOnAction(new ActionOption());
        monteurMenu.button_credit.setOnAction(new ActionCredit());
        monteurMenu.button_quit.setOnAction(new ActionRageQuit());
        vue.button_lvl_prec.setOnAction(new ActionLvlPrec());
        vue.button_lvl_next.setOnAction(new ActionLvlNext());

        monteurMenu.sliderSon.valueProperty().addListener(new ActionChangeVolume());

        listeNiveaux = new ArrayList<>();

        for (int i=0;i<controleur.getListeNiveaux().size(); i++){
            indiceNiveaux = i+1;
            String text_button = "niveau "+indiceNiveaux;
            Button button_lvl = new Button(text_button);
                button_lvl.setOnAction(new ActionGoToLvl());
            listeNiveaux.add(button_lvl);
        }

    }

    //les events pour le menu

    class ActionMenu implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            controleur.setLevel(0);
            reset.setVisible(false);
            undo.setVisible(false);
            redo.setVisible(false);
            replay.setVisible(false);
        }
    }

    class ActionStart implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            controleur.setLevel(1);
            reset.setVisible(true);
            undo.setVisible(true);
            redo.setVisible(true);
            replay.setVisible(true);

        }
    }

    class ActionSelect implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            vue.gridPane.getChildren().clear();
            vue.gridPaneHaut.getChildren().clear();
            vue.gridPane.setHgap(10);
            vue.gridPane.setVgap(10);
            Label label1 = new Label("Voici la liste des niveaux accessibles: ");
            vue.gridPaneHaut.add(label1,0,0);
            int cptrow = 1;
            int cptcol = 0;
            for (int i=0;i<listeNiveaux.size();i++){

                Label labelRecord = new Label("Record:"+controleur.getRecord(i));
                labelRecord.setId("labelRecord");

                if (cptcol<5){
                    vue.gridPane.add(listeNiveaux.get(i),cptcol,cptrow);
                    vue.gridPane.add(labelRecord,cptcol,cptrow+1);
                    cptcol+=1;
                }
                else{
                    cptcol=0;
                    cptrow+=2;
                    vue.gridPane.add(listeNiveaux.get(i),cptcol,cptrow);
                    vue.gridPane.add(labelRecord,cptcol,cptrow+1);
                    cptcol+=1;
                }
            }

            vue.monteurScene.ajoutBas(monteurMenu.button_menu);
            monteurMenu.button_menu.setVisible(true);
            System.out.println("menu de selection des niveaux");
            vue.button_lvl_next.setVisible(false);
            vue.button_lvl_prec.setVisible(false);
            reset.setVisible(false);
            undo.setVisible(false);
            redo.setVisible(false);
            replay.setVisible(false);
        }
    }


    class ActionOption implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            monteurMenu.setMenuOption();
            reset.setVisible(false);
            undo.setVisible(false);
            redo.setVisible(false);
            replay.setVisible(false);
        }
    }

    class ActionCredit implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            monteurMenu.setCredit();
            reset.setVisible(false);
            undo.setVisible(false);
            redo.setVisible(false);
            replay.setVisible(false);

        }
    }

    class ActionRageQuit implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            Platform.exit();
            System.exit(0);
        }
    }

    class ActionLvlPrec implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            controleur.setLevel(controleur.getLevel()-1);
        }
    }

    class ActionLvlNext implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            controleur.setLevel(controleur.getLevel()+1);
        }
    }

    class ActionGoToLvl implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event){
            Button button=(Button)event.getSource();
            int indice = Integer.parseInt(button.getText().substring(7));
            controleur.setLevel(indice);
            reset.setVisible(true);
            undo.setVisible(true);
            redo.setVisible(true);
            replay.setVisible(true);

        }
    }

    class ActionChangeVolume implements ChangeListener {
        public void changed(ObservableValue arg0, Object arg1, Object arg2) {
            for (int i=0;i<playlist.size();i++){
                playlist.get(i).setVolume(monteurMenu.sliderSon.getValue()/100);
            }
        }
    }

    class ActionReset implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event) {
            controleur.reset();
        }

    }

    class ActionUndo implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event) {
            controleur.undo();
        }

    }

    class ActionRedo implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event) {
            controleur.redo();
        }

    }

    class ActionReplay implements EventHandler<ActionEvent> {
        public void handle(ActionEvent event) {
            controleur.replay();
        }
    }

    public void addKeyHandler(Scene scene) {
        scene.setOnKeyPressed(ke -> {
            KeyCode keycode = ke.getCode();
            switch (keycode) {
                case UP:
                    if (controleur.getLevel()!=0){
                        vue.soko=(new ImageView(vue.sokoban[7]));
                        controleur.move(0, -1);
                        up = true;
                    }
                    System.out.println("UP Pressed");
                    break;
                case DOWN:
                    if (controleur.getLevel()!=0) {
                        vue.soko=(new ImageView(vue.sokoban[1]));
                        controleur.move(0, +1);
                        down = true;
                    }
                    System.out.println("DOWN Pressed");
                    break;
                case LEFT:
                    if (controleur.getLevel()!=0) {
                        vue.soko=(new ImageView(vue.sokoban[5]));
                        controleur.move(-1, 0);
                        left = true;
                    }
                    System.out.println("LEFT Pressed");
                    break;
                case RIGHT:
                    if (controleur.getLevel()!=0) {
                        vue.soko=(new ImageView(vue.sokoban[6]));
                        controleur.move(1, 0);
                        right = true;
                    }
                    System.out.println("RIGHT Pressed");
                    break;
            }
        });
    }
}