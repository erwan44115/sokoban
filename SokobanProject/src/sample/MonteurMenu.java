package sample;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class MonteurMenu {

    GridPane gridPane;
    GridPane gridPaneHaut;

    Button button_menu;
    Button button_start;
    Button button_select;
    Button button_tuto;
    Button button_option;
    Button button_credit;
    Button button_quit;

    Slider sliderSon;

    public MonteurMenu(GridPane paneCentre, GridPane paneHaut){
        gridPane = paneCentre;
        gridPaneHaut = paneHaut;

        button_menu = new Button("Retour au menu");
        button_start = new Button("Commencer le jeu");
        button_select = new Button("Choisir un niveau");
        button_option = new Button("Options");
        button_credit = new Button("Crédit");
        button_quit = new Button("Quitter");

        sliderSon = new Slider();
    }

    public void setMenu() {
        gridPane.getChildren().clear();
        gridPaneHaut.getChildren().clear();
        gridPane.setVgap(20);
        //Elements du menu
        button_start.setPrefWidth(200);
        button_select.setPrefWidth(200);
        button_option.setPrefWidth(200);
        button_credit.setPrefWidth(200);
        button_quit.setPrefWidth(200);
        Label label1 = new Label("Bienvenue sur le Sokoban");
        gridPaneHaut.add(label1,0,0);
        gridPane.add(button_start,0,1);
        gridPane.add(button_select,0,2);
        gridPane.add(button_option,0,3);
        gridPane.add(button_credit,0,4);
        gridPane.add(button_quit,0,5);

        button_menu.setVisible(false);


        System.out.println("menu principale");

    }


    public void setMenuOption(){
        gridPane.getChildren().clear();
        gridPaneHaut.getChildren().clear();
        Label label1 = new Label("Option:");
        gridPaneHaut.add(label1,0,0);

        Label labelVolume = new Label("Volume :                                                           ");

        labelVolume.setId("labelVolume");

        sliderSon.setMin(0);
        sliderSon.setMax(100);
        sliderSon.setValue(20);
        sliderSon.setShowTickLabels(true);
        sliderSon.setShowTickMarks(true);
        sliderSon.setMinorTickCount(0);
        sliderSon.setMajorTickUnit(10);
        sliderSon.setSnapToTicks(true);

        gridPane.add(labelVolume,0,1);
        gridPane.add(sliderSon,0,2);
        button_menu.setVisible(true);
        System.out.println("menu des options");
    }

    public void setCredit(){
        gridPane.getChildren().clear();
        gridPaneHaut.getChildren().clear();
        Text txtcredit = new Text();
        txtcredit.setText("Ce Sokoban a été réalisé par: Erwan, Nathan, Paul, Quentin");
        txtcredit.setId("credit");
        gridPane.add(txtcredit,0,0);
        button_menu.setVisible(true);
        System.out.println("les crédits");
    }
}
