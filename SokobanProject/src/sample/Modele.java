package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface Modele {
    public int[][] getEtat();
    public void move(int x, int y);
    public void reset();
    public void undo();
    public void redo();
    public void replay(int indiceEtat);
    public void loadLevel(String chemin);
    public Map<Integer,List<List<String>>> getListeNiveaux();
    public void setNiveauActuel(int level);
    public  List<List<String>> getNiveauActuel();
    public Boolean isWin();
    public int getSokoPositionX();
    public int getSokoPositionY();
    public int getLevel();
    public void incNbCoup();
    public int getNbCoup();
    public ArrayList<String> getListNbCoup();
    public String getRecord(int val);
    public void setNbCoup(int ind,int val);
    public int getNumeroLevelActuel();
    public Boolean getVictoire();

}
