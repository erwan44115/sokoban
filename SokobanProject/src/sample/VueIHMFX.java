package sample;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import javafx.geometry.HPos;

public class VueIHMFX {

    GridPane gridPane = new GridPane();
    GridPane gridPaneHaut = new GridPane();

    /* montage de la scene */

    Button button_lvl_prec = new Button("Niveau précédent");
    Button button_lvl_next = new Button("Niveau suivant");

    MonteurScene monteurScene = new MonteurScene();
    MonteurMenu monteurMenu = new MonteurMenu(gridPane,gridPaneHaut);

    ImageView soko;

    CommandeTabInt commandeGetEtat;

    Controleur c;
    Image[] sokoban = new Image[]{
            new Image(new FileInputStream(
                    "images/Wall_Brown.png"),59,59,true,false),
            new Image(new FileInputStream(
                    "images/Character4.png"),59,59,true,false),
            new Image(new FileInputStream(
                    "images/CrateDark_Black.png"),59,59,true,false),
            new Image(new FileInputStream(
                    "images/EndPoint_Black.png"),32,32,true,false),
            new Image(new FileInputStream(
                    "images/CrateDark_Red.png"),59,59,true,false),
            new Image(new FileInputStream(
                    "images/Character1.png"),59,59,true,false),
            new Image(new FileInputStream(
                    "images/Character2.png"),59,59,true,false),
            new Image(new FileInputStream(
                    "images/Character7.png"),59,59,true,false)
    };


    public VueIHMFX(Controleur controleur) throws FileNotFoundException {
        c=controleur;
        commandeGetEtat = controleur.commandeGetEtat();
        soko= new ImageView(sokoban[1]);
        dessine();
        controleur.loadLevel("MicroCosmos.txt");
        System.out.println("NIVEAU ENTIER : ");
        System.out.println(controleur.facadeModele.getListeNiveaux());
      /*  System.out.println("LEVEL 2 CHARGÉ : ");
        System.out.println(controleur.facadeModele.getNiveauActuel());
        System.out.println(controleur.facadeModele.getSokoPositionX());
        System.out.println(controleur.facadeModele.getSokoPositionY());
    */}


    public void dessine() {
        gridPane.getChildren().clear();
        gridPaneHaut.getChildren().clear();
        gridPane.setVgap(0);
        gridPane.setHgap(0);
        List<List<String>> etat = commandeGetEtat.exec();
        if(c.getVictoire()==true){
            if (c.getRecord(c.getLevel()-1)=="Non réalisé"){
                c.setNbCoup(c.getLevel()-1,c.getnbCoup());
            }
            else if (Integer.parseInt(c.getRecord(c.getLevel()-1))>c.getnbCoup()) {
                c.setNbCoup(c.getLevel()-1,c.getnbCoup());
            }

            Label label = new Label("Vous avez gagné en "+c.getnbCoup()+" coups !");
            gridPaneHaut.add(label,0,1);
        }

        if (c.getLevel()==0){
            monteurMenu.setMenu();
            button_lvl_next.setVisible(false);
            button_lvl_prec.setVisible(false);
        }
        else{
            Label labelMission = new Label("Place les caisses sur les emplacements       ");
            gridPaneHaut.add(labelMission,0,0);
            gridPaneHaut.add(new Label("Nombres de coup :"+c.getnbCoup()+""),1,0);
            gridPaneHaut.add(new Label("Record : "+c.getRecord(c.getLevel()-1)+""),1,1);

            for (int i = 0; i < etat.size() ; i++) {
                for (int j = 0; j < etat.get(i).size() ; j++) {
                    if (etat.get(i).get(j).equals("#")) {
                        gridPane.add(new ImageView(sokoban[0]), j, i);
                    }
                    else if (etat.get(i).get(j).equals("@") || etat.get(i).get(j).equals("+")){
                        gridPane.add(soko, j, i);
                        gridPane.setHalignment(soko, HPos.CENTER);
                    }
                    else if (etat.get(i).get(j).equals("$")){
                        gridPane.add(new ImageView(sokoban[2]), j, i);
                    }else if (etat.get(i).get(j).equals(".")){
                        ImageView img = new ImageView(sokoban[3]);
                        gridPane.add(img, j, i);
                        gridPane.setHalignment(img, HPos.CENTER);
                    }
                    else if(etat.get(i).get(j).equals("*")){
                        gridPane.add(new ImageView(sokoban[4]), j, i);
                    }
                }
            }
            if (c.getLevel()==1){
                button_lvl_prec.setVisible(false);
                button_lvl_next.setVisible(true);
            }
            else if (c.getLevel()==c.getListeNiveaux().size()){
                button_lvl_prec.setVisible(true);
                button_lvl_next.setVisible(false);
            }
            else {
                button_lvl_next.setVisible(true);
                button_lvl_prec.setVisible(true);
            }
            monteurMenu.button_menu.setVisible(true);

        }

        System.out.println("NOUVEAU DESSIN :");
        System.out.println(c.facadeModele.getNiveauActuel());
    }
}
